
console.log(`ACTIVITY : TODO LIST`)
console.log(`Item 3-4`);
console.log(fetch('https://jsonplaceholder.typicode.com/todos'));
fetch('https://jsonplaceholder.typicode.com/todos',{
	method:'GET'
})
.then(response => response.json())
.then(data => {
	let array = data.map(function(data){
	return data.title})
	console.log(array);
})


console.log(`Item 5-6`);
fetch('https://jsonplaceholder.typicode.com/todos',{
	method:'GET'
})
.then(response => response.json())
.then(data => {
		let specificToDo = 'quo laboriosam deleniti aut qui';
		// console.log (data[0].title)
			for(let i = 0 ; i < data.length; i++) {
			if(data[i].title == specificToDo){
			console.log(`The item ${data[i].title} on the list has a status of ${data[i].completed}`);
			}}
		}
	);

//ITEM 7
fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userID: 201
	})
})
.then(response => response.json())
.then(json => console.log(json))


//ITEM 8
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'updated To Do List Item for Activity Item 8',
		completed: false,
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))

//ITEM 9

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))

//ITEM 10

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "Pending",
		id: 1,
		status: "Pending",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))

//ITEM 11
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		completed: true,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))


//ITEM 12
fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))

